## Aller Media Front-end assignment

# Instructions

Your task is to create or implement a responsive HTML slider, in which at least two slides contain adverts served from Google's DFP network. You may either create the actual slider yourself, or preferably implement a library of your choosing via Bower. The advert in question may only be loaded (via the googletag.display() function) when its slide is actually visible. Fork this repository to get started, and then push your changes to a hosted git repo of your choosing. 

# What we want

* Styling via SASS, compiled into a minified version as well as a single debug version
* All javascript compiled into a single minified file on top of the regular source file(s), including possible vendor scripts
* Support for the latest two versions of Chrome, FireFox, Safari and Internet Explorer
* Touch/swipe support for mobile

# Bonus points

* For using a task runner
* For not using jQuery
* For writing wise-ass comments about vague product specs
* For only loading the small version of the image on ~320px screens

# What you get

* A photoshop mock-up and image source files
* This repository, containing a small starting point for loading the DFP testing ads, showing our general JS coding standard, as well as empty Bower and NPM package files for listing possible dependencies

# Preferred tools
* [SASS](http://sass-lang.com/)
* [Bower](http://bower.io/)
* [Gulp](http://gulpjs.com/)

Take the time you need and don't reinvent the wheel if you don't have to. This assignment is about maintaining a good project structure and writing easily read code and markup for multiple devices.