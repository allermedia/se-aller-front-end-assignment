/**
 * GoogleTag starting point
 */
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
  var gads = document.createElement("script");
  gads.async = true;
  gads.type = "text/javascript";
  var useSSL = "https:" == document.location.protocol;
  gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";
  var node =document.getElementsByTagName("script")[0];
  node.parentNode.insertBefore(gads, node);
 })();



/**
 * Feel free to extend or modify this to enable dynamically displaying the DFP ad, by
 * pushing the display function to the googletag.cmd array like in /index.html
 */
function AdsManager() {}

AdsManager.prototype.defineNewSlot = function(id)
{
  googletag.cmd.push(function() {
    var newSlot = googletag.defineSlot('/6355419/Travel/Europe/France/Paris',[300, 250], id);
    newSlot.addService(googletag.pubads());
  });
}

AdsManager.prototype.init = function()
{
  googletag.cmd.push(function() {

    //Let's define our first slot right off the bat
    //To include more ad slots, just call the defineNewSlot prototype method 
    //with the ID of the div to load the ad in. 
    AdsManager.prototype.defineNewSlot('dfpTestAd');
    
    googletag.pubads().enableAsyncRendering();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();

  });
}

AdsManager.prototype.init();